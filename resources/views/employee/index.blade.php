@extends('layouts.main')
@section('content-body')

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Employee Page</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{asset('/')}}">Home</a></li>
              <li class="breadcrumb-item active">Employee</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

        <div class="row">
         
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h5 class="m-0 card-title">Employee Table</h5>
                <div class="card-tools">
                  <a href="{{asset('employee/create')}}" class="btn btn-primary">Create Employee</a>
                </div>
              </div>
              <div class="card-body">

                <div class="row">
                  <div class="col-lg-12">
                    
                  </div>
                </div>


                <div class="row form-group">
                  <div class="col-lg-1">
                    <label for="">Age : </label>
                  </div>
                  <div class="col-lg-3">
                    <select name="" id="ageselect" class="form-control">
                      <option value="">-- select age --</option>
                      @foreach($age as $e)
                        <option value="{{$e->age}}">{{$e->age}}</option>
                      @endforeach
                    </select>
                    
                  </div>
                  <div class="col-lg-1">
                    <label for="">Salary : </label>
                  </div>
                  <div class="col-lg-3">
                    <select name="" id="salaryselect" class="form-control">
                      <option value="">-- select salary --</option>
                      @foreach($salary as $e)
                        <option value="{{$e->salary}}">{{$e->salary}}</option>
                      @endforeach
                    </select>
                    
                  </div>
                  
                </div>
                <table class="table table-bordered" id="emptable">
                  <thead class="text-center">
                    <tr>
                      <th>#</th>
                      <th>action</th>
                      <th>picture</th>
                      <th>title name</th>
                      <th>emp name</th>
                      <th>birthday</th>
                      <th>age</th>
                      <th>salary</th>
                    </tr>
                  </thead>
                  
                </table>
              </div>

            </div>

            

          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection

@section('scripts')
<link rel="stylesheet" href="{{url('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.css')}}">
<link rel="stylesheet" href="{{url('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{url('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">

<script src="{{url('adminlte/plugins/datatables/jquery.dataTables.js?v=7.0.5')}}"></script>

<!-- DataTables  & Plugins -->
<script src="{{url('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{url('adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('adminlte/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{url('adminlte/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{url('adminlte/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{url('adminlte/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{url('adminlte/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{url('adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">

  $(document).ready(function() {      
      $.fn.dataTable.ext.errMode = 'throw';
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var tablecompare = $('#emptable').DataTable({
          pageLength: 3,
          responsive: true,
          processing: true,
          serverSide: true,
          dom: `<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>
              <'row'<'col-sm-12'tr>>
              <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
          //dom : 'Bfrtip' ,
          autoWidth : true,
          lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50,100, "All"]] ,   
          lengthChange : false   ,             
          //order: [[5, 'asc']],
          ajax: {
          url: "{{url('employee/employeetable')}}",
          
          },
        
        buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"] ,
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false , width:'30px', className:'text-center'},
            {data: 'action', name:'action', width:'100px', className:'text-center' , orderable: false} ,
            {data: 'picture', name:'picture' , className: 'text-center' , orderable: false , width:'120px'} ,
            {data: 'titlename', name: 'titlename', className:'text-center'} ,
            {data: 'empname', name: 'empname', className:'text-center'} , 
            {data: 'birthday', name: 'birthday', className:'text-center'} ,
            {data: 'age', name: 'age', className: 'text-center'} ,
            {data: 'salary' , name: 'salary' , className: 'text-center'}

        ]
    });

$('#ageselect').on('change', function () {
      tablecompare.columns(6).search( this.value ).draw();
});

$('#salaryselect').on('change', function () {
      tablecompare.columns(7).search( this.value ).draw();
});

$('body').on('click', '.deleteEmp', function () {

  var emp_id = $(this).data("id");

  Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
  $.ajax({
      type: "POST",
      url: "{{url('employee')}}/"+emp_id+"/delete" ,
      success: function (data) {
          tablecompare.draw();
          Swal.fire({
            position: "center",
            icon: "success",
            title: "ลบข้อมูลเรียบร้อย",
            showConfirmButton: true,
            timer: 3000
        });
      },
      error: function (data) {
          console.log('Error:', data);
      }
  });
  }
})
    
});

  });

</script>    
@endsection