@extends('layouts.main')
@section('content-body')


    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Employee Page</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{asset('/')}}">Home</a></li>
                <li class="breadcrumb-item">Employee</li>
                <li class="breadcrumb-item active">Create</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
           
            <div class="col-lg-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="m-0 card-title">Employee Create</h5>
                  <div class="card-tools">
                    <a href="{{asset('employee')}}" class="btn btn-primary">Back</a>
                  </div>
                </div>
                <div class="card-body">
                  
<form action="{{asset('employee/store')}}" method="POST" enctype="multipart/form-data">
@csrf

<div class="row form-group">
    <div class="col-md-12">
        <div class="form-group">
            <input type="file" name="picture" placeholder="Choose image" id="image">
            @error('picture')
             <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
        </div>
    </div>
      
    <div class="col-md-12 mb-2">
        <img id="preview-image" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
            alt="preview image" style="max-height: 250px;">
    </div>
</div>

<div class="row form-group">
    <div class="col-lg-4 pt-3">
        <label for="">Title Name : </label>
        <input type="text" name="titlename" placeholder="titlename" class="form-control">
        @error('titlename')
            <div class="error text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-lg-4 pt-3">
        <label for="">Employee Name : </label>
        <input type="text" name="empname" placeholder="empname" class="form-control">
        @error('empname')
            <div class="error text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-lg-4 pt-3">
        <label for="">Birthday : </label>
        <input type="date" name="birthday" placeholder="birthday" class="form-control">
    </div>
    <div class="col-lg-4 pt-3">
        <label for="">Age</label>
        <input type="text" name="age" placeholder="age" class="form-control">
    </div>
    <div class="col-lg-4 pt-3">
        <label for="">Salary</label>
        <input type="text" name="salary" placeholder="salary" class="form-control">
    </div>
</div>

<div class="row form-group">
    <div class="col-lg-12">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>

</form>

                </div>
  
              </div>
  
              
  
            </div>
            <!-- /.col-md-6 -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
  

@endsection

@section('scripts')
<script type="text/javascript">
    $('#image').change(function(){
           
    let reader = new FileReader();
    reader.onload = (e) => { 
      $('#preview-image').attr('src', e.target.result); 
    }
    reader.readAsDataURL(this.files[0]);
   });

  </script>
@endsection