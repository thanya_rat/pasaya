<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use \File;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employeeTable()
    {
        $emp = DB::table('employee')->orderBy('id','desc')->get();

        //return Datatables::of(Employee::orderByDesc('id')->get())
        return Datatables::of($emp)
        ->addIndexColumn()
        ->editColumn('action', function($emp) {
            return '           
                <a href="'. url('employee/edit') .'/'.$emp->id.' " class="btn btn-outline-primary btn-flat editEmp" title="Edit details" data-id="'. $emp->id .'">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="javascript:;" class="btn btn-outline-danger btn-flat deleteEmp" title="Delete" data-id="'. $emp->id .'">
                    <i class="fa fa-trash"></i>
                </a>
        ';
        })
        ->editColumn('picture', function($emp) {
            
            return '<img src="'. url('../storage/app') . "/". $emp->picture .'" width="120">';
        })
        ->editColumn('birthday', function($emp) {
            if($emp->birthday == null) {
                return '';
            }else{
                return '<div class="badge bg-success">' .date('Y-m-d',strtotime($emp->birthday)). ' </div>' ;
            }
            
        })
        ->rawColumns(['action','picture','birthday'])
        ->make(true);
    }

    public function index()
    {
        //
        $age = DB::table('employee')->select('age')->distinct()->where('age','!=',null)->get();
        $salary = DB::table('employee')->select('salary')->distinct()->where('salary','!=',null)->get();

        return view('employee.index', compact('age','salary'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([

            'titlename' => 'required',
            'empname' => 'required',
            'picture' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
 
                ]);

        if ($request->hasFile('picture')) {
        $name = $request->file('picture')->getClientOriginalName();
        $path = $request->file('picture')->store('uploads');
        } else {
            $path = '';
        }

        $emp    = new Employee;
        $emp->titlename = $request->titlename;
        $emp->empname   = $request->empname;
        $emp->picture   = $path;
        $emp->birthday  = $request->birthday;
        $emp->age       = $request->age ;
        $emp->salary    = $request->salary;
        

        $emp->save();

        //return response()->json(['message'=>'success']);

        return back()->with('success','Item Create Successful!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $emp = Employee::find($id);

        return view('employee.edit', compact('emp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        request()->validate([

            'titlename' => 'required',
            'empname' => 'required',
            
                ]);

        if ($request->hasFile('picture')) {
        $name = $request->file('picture')->getClientOriginalName();
        $path = $request->file('picture')->store('uploads');

        $emp    = Employee::findOrFail($id);
        $emp->titlename = $request->titlename;
        $emp->empname   = $request->empname;
        $emp->picture   = $path;
        $emp->birthday  = $request->birthday;
        $emp->age       = $request->age ;
        $emp->salary    = $request->salary;

        }else{
        $emp    = Employee::findOrFail($id);
        $emp->titlename = $request->titlename;
        $emp->empname   = $request->empname;
        $emp->birthday  = $request->birthday;
        $emp->age       = $request->age ;
        $emp->salary    = $request->salary;
        }
        $emp->save();
        //return response()->json(['message'=>'success']);
        return back()->with('success','Item Create Successful!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Employee::find($id);
        if($data->picture != null) {
            
            unlink(storage_path('app/'.$data->picture));
        }
        $data->delete();

        

        return response()->json(['success'=>'Customer deleted!']);
    }
}
