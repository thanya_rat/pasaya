<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table = 'employee'; //กำหนมดชื่อตาราง
    protected $fillable = ['titlename','empname','birthday','age','salary','picture']; // ชื่อฟิลด์
}
