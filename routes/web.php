<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [EmployeeController::class , 'index']);


//Route::get('/welcome', [WelcomeController::class, 'index']);

Route::post('/employee/store',[EmployeeController::class , 'store']);
Route::get('/employee', [EmployeeController::class , 'index']);
Route::get('/employee/create', [EmployeeController::class , 'create']);
Route::get('/employee/edit/{id}', [EmployeeController::class , 'edit']);

Route::get('employee/employeetable',[EmployeeController::class,'employeeTable']);
Route::post('employee/update/{id}',[EmployeeController::class,'update']);
Route::post('employee/{id}/delete', [EmployeeController::class, 'destroy']);

// Route::get('/layouts/main', function() {
//     return view('layouts.main');
// });